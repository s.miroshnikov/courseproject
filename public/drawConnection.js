let canvas = document.getElementById('canvas')
let context = canvas.getContext('2d')

let width = canvas.width = innerWidth-17
let height = canvas.height = innerHeight+40

let addRelation = document.getElementById(`addRelation`);
let addRelationCreationModal = document.getElementById(`addRelationCreationModal`);
let spanRelationCreationModal = document.getElementById(`closeRelationCreationModal`);
let relationName = document.getElementById(`relationCreationModalName`);

let relationClassList = document.getElementById("relationClassList");

let countOfStroke;

addRelation.onclick = function (){
    countOfStroke = 0
    addRelationCreationModal.style.display = "block";
    document.body.style.position = 'fixed';
    document.body.overflowY = 'hidden';

    relationName.focus();

    while (relationClassList.firstChild) {
        relationClassList.removeChild(relationClassList.firstChild);
    }

    let tr = document.createElement("tr");
    let thName = document.createElement("th");
    let thType = document.createElement("th");

    thName.innerHTML = "Имена классов";
    thType.innerHTML = "Типы классов"
    tr.appendChild(thName);
    tr.appendChild(thType);
    relationClassList.appendChild(tr);

    for(let i = 1; i < localStorage.getItem('x'); i++) {
        let tr = document.createElement("tr");
        tr.setAttribute('id', `${i}`);
        let tdName = document.createElement("td");
        let tdType = document.createElement("td");

        tdName.innerHTML = localStorage.getItem(`name ${i}`);
        tdType.innerHTML = localStorage.getItem(`Type ${i}`);
        tr.appendChild(tdName);
        tr.appendChild(tdType);
        relationClassList.appendChild(tr);
    }

    relationClassList.onclick = function(e){
        let stroke = e.target.closest('tr');

        if (!stroke.classList.contains('selected') && countOfStroke !== 2) {
            stroke.classList.toggle('selected');
            countOfStroke++;
        }else if(stroke.classList.contains('selected')){
            stroke.classList.remove('selected');
            countOfStroke--;
        }
    };
}
spanRelationCreationModal.onclick = function() {
    addRelationCreationModal.style.display = "none";

    const wrapObj = document.querySelectorAll('.selected');
    for(let i = 0; i<wrapObj.length;i++) {
        wrapObj[i].classList.remove('selected');
    }
}

let modifyRelationModal = document.getElementById('modifyRelationModal');
let modifyRelation = document.getElementById('modifyRelation');
let spanRelationModifyModal = document.getElementById('closeRelationModifyModal');
modifyRelation.onclick = function() {
    modifyRelationModal.style.display = 'block';
}
spanRelationModifyModal.onclick = function() {
    modifyRelationModal.style.left = 0 + 'px';
    modifyRelationModal.style.top = 0 + 'px';
    modifyRelationModal.style.display = 'none';
}

let createRelationButton = document.getElementById('saveRelation');
let currentId;
createRelationButton.addEventListener('click', createLineBetweenRelation);

let saveRelationInModal = document.getElementById(`saveRelationInModal`);
let swapOrientation = document.getElementById(`swapOrientation`);
let deleteRelation = document.getElementById(`deleteRelation`);

let elements = [], el, relationBetween = [];
let RB, nameOfRelation, stringRelation;

function createLineBetweenRelation() {
    for (let i = 0; i < document.getElementsByClassName('selected').length; i++) {
        currentId = document.getElementsByClassName('selected')[i].id;
        el = document.getElementById(`Concept ${currentId}`);

        relationBetween[i] = el.id.match(/\d+/);
    }

    console.log(relationBetween[0][0]);

    RB++;

    if (document.getElementById(`relationCreationModalName`).value !== '') {
        localStorage.setItem(`RelationName ${RB}`, `${document.getElementById(`relationCreationModalName`).value}`);
        nameOfRelation = document.getElementById(`relationCreationModalName`).value;
    } else {
        localStorage.setItem(`RelationName ${RB}`, `Relation ${RB}`);
        nameOfRelation = `Relation ${RB}`;
    }

    stringRelation = `<div id="relationName ${RB}" class="relation">${nameOfRelation}</div>`;
    form.insertAdjacentHTML("beforeend", stringRelation);

    let RN = document.getElementById(`relationName ${RB}`);
    let RNColor = 'red';
    let xInBet1, xInBet2, yInBet1, yInBet2, wInBet1, wInBet2, hInBet1, hInBet2;
    let mediumLengthBetween, mediumHeightBetween;
    xInBet1 = document.getElementById(`Concept ${relationBetween[0][0]}`).getBoundingClientRect().x;
    xInBet2 = document.getElementById(`Concept ${relationBetween[1][0]}`).getBoundingClientRect().x;
    yInBet1 = document.getElementById(`Concept ${relationBetween[0][0]}`).getBoundingClientRect().y;
    yInBet2 = document.getElementById(`Concept ${relationBetween[1][0]}`).getBoundingClientRect().y;
    wInBet1 = Math.round(document.getElementById(`Concept ${relationBetween[0][0]}`).getBoundingClientRect().width);

    RN.style.position = 'absolute';
    RN.style.position = 'absolute';
    if (xInBet1 + wInBet1 > xInBet2) {
        mediumLengthBetween = Math.abs((xInBet1 - xInBet2 + wInBet1)/2);
        RN.style.left = Math.round(xInBet1+ wInBet1 - mediumLengthBetween - RN.getBoundingClientRect().width/2) + 'px';
    } else if (xInBet1 + wInBet1 < xInBet2) {
        mediumLengthBetween = Math.abs((xInBet1 - xInBet2 + wInBet1)/2);
        RN.style.left = Math.round(xInBet1 + wInBet1 + mediumLengthBetween - RN.getBoundingClientRect().width/2) + 'px';
    }

    mediumHeightBetween = Math.abs(yInBet1 - yInBet2);
    if (yInBet1 > yInBet2) {
        RN.style.top = Math.round(yInBet2 + mediumHeightBetween/2) + 'px';
    } else if (yInBet1 < yInBet2) {
        RN.style.top = Math.round(yInBet1 + mediumHeightBetween/2) + 'px';
    }


    localStorage.setItem(`RelationNamePosX ${RB}`, `${RN.style.left}`);
    localStorage.setItem(`RelationNamePosY ${RB}`, `${RN.style.top}`);
    localStorage.setItem(`RelationNameColor ${RB}`, `${RNColor}`);
    localStorage.setItem(`RelationBetween ${RB}`, `${relationBetween}`);
    localStorage.setItem(`countOfRelation`, RB);

    const wrapObj = document.querySelectorAll('.selected');
    for(let i = 0; i<wrapObj.length;i++) {
        wrapObj[i].classList.remove('selected');
    }

    context.clearRect(0, 0, width, height)

    connect();

    addRelationCreationModal.style.display = "none";
}

/*------------------------------------*/

if(localStorage.getItem(`countOfRelation`)!==null) {
    RB = parseInt(localStorage.getItem(`countOfRelation`));
}  else {
    RB = 0;
}

function moveName() {
    for (let i = 1; i < RB + 1; i++) {
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j];
            elements[j + 1] = {
                x: document.getElementById(`Concept ${temp}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${temp}`).getBoundingClientRect().y,
                w: document.getElementById(`Concept ${temp}`).getBoundingClientRect().width,
                h: document.getElementById(`Concept ${temp}`).getBoundingClientRect().width,
            }
        }

        let RN, mediumLengthBetween, mediumHeightBetween;
        RN = document.getElementById(`relationName ${i}`);
        if (elements[1].x + elements[1].w > elements[2].x) {
            mediumLengthBetween = Math.abs((elements[1].x - elements[2].x + elements[1].w)/2);
            RN.style.left = Math.round(elements[1].x + elements[1].w - mediumLengthBetween - RN.getBoundingClientRect().width / 2) + 'px';
        } else if (elements[1].x + elements[1].w < elements[2].x) {
            mediumLengthBetween = Math.abs((elements[1].x - elements[2].x + elements[1].w)/2);
            RN.style.left = Math.round(elements[1].x + elements[1].w + mediumLengthBetween - RN.getBoundingClientRect().width / 2) + 'px';
        }

        if (elements[1].y > elements[2].y) {
            mediumHeightBetween = Math.abs(elements[1].y - elements[2].y);
            RN.style.top = Math.round(elements[2].y + mediumHeightBetween / 2) + 'px';
        } else if (elements[1].y < elements[2].y) {
            mediumHeightBetween = Math.abs(elements[1].y - elements[2].y);
            RN.style.top = Math.round(elements[1].y + mediumHeightBetween / 2) + 'px';
        }

        localStorage.setItem(`RelationNamePosX ${i}`, `${RN.style.left}`);
        localStorage.setItem(`RelationNamePosY ${i}`, `${RN.style.top}`);
    }
}

function connect() {
    context.clearRect(0, 0, width, height)

    for (let i = 1; i < RB + 1; i++) {
        closed = 0;
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j];
            elements[j + 1] = {
                x: document.getElementById(`Concept ${temp}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${temp}`).getBoundingClientRect().y,
                id: temp
            }
        }
        drawLine(
            elements[1].x,
            elements[2].x,
            elements[1].y - 73,
            elements[2].y - 73,
            elements[1].id,
        )
    }
}

let angleCoefficient = [], constB = [];
let x1,x2,y1,y2, width1, width2, height1, height2;
let temp;

function DL(RelationID) {
    for (let i = 1; i < RB + 1; i++) {
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${RelationID}`).split(',')[j]
            console.log(temp);
            elements[j + 1] = {
                x: document.getElementById(`Concept ${temp}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${temp}`).getBoundingClientRect().y,
                w: document.getElementById(`Concept ${temp}`).getBoundingClientRect().width,
                h: document.getElementById(`Concept ${temp}`).getBoundingClientRect().height
            }
        }
        x1 = elements[1].x;
        x2 = elements[2].x;
        y1 = elements[1].y - 73;
        y2 = elements[2].y - 73;
        width1 = elements[1].w;
        width2 = elements[2].w;
        height1 = elements[1].h;
        height2 = elements[2].h;
        drawOpacityLine(x1,x2,y1,y2,width1, width2, height1, height2);
    }
}


let relationModalName = document.getElementById(`relationName`);
canvas.oncontextmenu = function(e) {
    for (let i = 1; i < RB + 1; i++) {
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j]
            console.log(temp);
            elements[j + 1] = {
                x: document.getElementById(`Concept ${temp}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${temp}`).getBoundingClientRect().y,
                w: document.getElementById(`Concept ${temp}`).getBoundingClientRect().width,
                h: document.getElementById(`Concept ${temp}`).getBoundingClientRect().height
            }
        }
        x1 = elements[1].x + elements[1].w + 4;
        x2 = elements[2].x - 10;
        y1 = elements[1].y + elements[1].h/2;
        y2 = elements[2].y + elements[2].h/2;
        angleCoefficient[i] = parseFloat(((y2 - y1) / (x2 - x1)).toFixed(10));
        constB[i] = parseFloat(((x2 * y1 - y2 * x1) / (x2 - x1)).toFixed(10));
        if ((e.y > Math.round(e.x * angleCoefficient[i] + constB[i]) - 5
            && e.y < Math.round(e.x * angleCoefficient[i] + constB[i]) + 5
            && e.x > x1 && e.x < x2) || (e.y > Math.round(e.x * angleCoefficient[i] + constB[i]) - 5
            && e.y < Math.round(e.x * angleCoefficient[i] + constB[i]) + 5
            && e.x > x2 && e.x < x1)) {
            modifyRelationModal.style.display = 'block';
            relationModalName.value = localStorage.getItem(`RelationName ${i}`)

            let firstElement, secondElement, numbersOfElementsInRelation, textLine;
            numbersOfElementsInRelation = localStorage.getItem(`RelationBetween ${i}`).split(',');
            firstElement = localStorage.getItem(`name ${numbersOfElementsInRelation[0]}`);
            secondElement = localStorage.getItem(`name ${numbersOfElementsInRelation[1]}`);

            textLine = firstElement + " и " + secondElement + " находятся в отношении " + "\""+localStorage.getItem(`RelationName ${i}`) + "\"";

            let writeDescription = document.getElementById("writeDescription");
            writeDescription.textContent = textLine;

            saveRelationInModal.onclick = function(){
                let relationName = document.getElementById(`relationChangeModalName`).value;
                if(relationName!=='')
                {
                    document.getElementById(`relationName ${i}`).textContent = relationName;
                    localStorage.setItem(`RelationName ${i}`, `${relationName}`);

                    document.getElementById(`relationChangeModalName`).value = '';

                    moveName();

                    modifyRelationModal.style.left = 0 + 'px';
                    modifyRelationModal.style.top = 0 + 'px';
                    modifyRelationModal.style.display = 'none';
                }else{
                    modifyRelationModal.style.left = 0 + 'px';
                    modifyRelationModal.style.top = 0 + 'px';
                    modifyRelationModal.style.display = 'none';
                }
            }

            swapOrientation.onclick = function(){
                let massiveOfTempRelation = [], massiveOfRelation = [];

                massiveOfRelation = localStorage.getItem(`RelationBetween ${i}`).split(`,`);
                massiveOfTempRelation[0] = massiveOfRelation[1]
                massiveOfTempRelation[1] = massiveOfRelation[0]

                localStorage.setItem(`RelationBetween ${i}`, `${massiveOfTempRelation}`);
                connect();
                moveName();

                modifyRelationModal.style.left = 0 + 'px';
                modifyRelationModal.style.top = 0 + 'px';
                modifyRelationModal.style.display = 'none';
            }

            return false;
        }
    }
}

modifyRelationModal.onmousedown = function (e) {
    if (e.which === 1) {
        let coords = getCoords(modifyRelationModal);
        let shiftX = e.pageX - coords.left;
        let shiftY = e.pageY - coords.top;

        moveAt(e);

        modifyRelationModal.style.zIndex = '1000';

        function moveAt(e) {
            modifyRelationModal.style.left = e.pageX - shiftX + 'px';
            modifyRelationModal.style.top = e.pageY - shiftY + 'px';
        }

        document.onmousemove = function (e) {
            if((e.x - 480 > modifyRelationModal.getBoundingClientRect().x && e.y - 100 > modifyRelationModal.getBoundingClientRect().y)
                && (e.x - 480 < modifyRelationModal.getBoundingClientRect().x + 960 && e.y - 100 < modifyRelationModal.getBoundingClientRect().y + 68)) {
                moveAt(e);
            }
        };

        modifyRelationModal.onmouseup = function () {
            document.onmousemove = null;
            modifyRelationModal.onmouseup = null;
        };

    }
    modifyRelationModal.ondragstart = function () {
        return false;
    };

    function getCoords(elem) {
        let box = elem.getBoundingClientRect();
        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        }
    }
}

canvas.onmousemove = function(e) {
    for (let i = 1; i < RB + 1; i++) {
        for (let j = 0; j < 2; j++) {
            temp = localStorage.getItem(`RelationBetween ${i}`).split(',')[j]
            console.log(temp);
            elements[j + 1] = {
                x: document.getElementById(`Concept ${temp}`).getBoundingClientRect().x,
                y: document.getElementById(`Concept ${temp}`).getBoundingClientRect().y,
                w: document.getElementById(`Concept ${temp}`).getBoundingClientRect().width,
                h: document.getElementById(`Concept ${temp}`).getBoundingClientRect().height
            }
        }
        x1 = elements[1].x + elements[1].w + 4;
        x2 = elements[2].x - 10;
        y1 = elements[1].y + elements[1].h/2;
        y2 = elements[2].y + elements[2].h/2;
        angleCoefficient[i] = parseFloat(((y2 - y1) / (x2 - x1)).toFixed(10));
        constB[i] = parseFloat(((x2 * y1 - y2 * x1) / (x2 - x1)).toFixed(10));

        if ((e.y > Math.round(e.x * angleCoefficient[i] + constB[i]) - 5
            && e.y < Math.round(e.x * angleCoefficient[i] + constB[i]) + 5
            && e.x > x1 && e.x < x2) || (e.y > Math.round(e.x * angleCoefficient[i] + constB[i]) - 5
            && e.y < Math.round(e.x * angleCoefficient[i] + constB[i]) + 5
            && e.x > x2 && e.x < x1)) {
            console.log(i);
            DL(i);
            break;
        }else {
            connect();
        }
    }
}

function drawOpacityLine(x1, x2, y1, y2, w1, w2, h1, h2) {
    context.clearRect(0, 0, width, height)
    context.beginPath()

    context.lineWidth = 3;

    context.globalAlpha = 0.7;

    context.strokeStyle = "blue";
    context.moveTo(x1 + w1, y1 + h1/2)

    context.lineTo(x2, y2 + h2/2)
    context.stroke()
}

function drawLine(x1, x2, y1, y2, id) {
    context.beginPath();

    context.lineWidth = 2;
    context.globalAlpha = 1.0;
    context.strokeStyle = "black";
    context.arc(x1 + parseInt(localStorage.getItem(`width ${id}`)) + 4, y1 + 20, 4, 0, 2 * Math.PI, false);
    context.fill();
    context.moveTo(x1 + parseInt(localStorage.getItem(`width ${id}`)) + 4, y1 + 20);

    context.lineTo(x2-10, y2 + 20)

    context.moveTo(x2, y2 + 20);
    context.lineTo(x2 - 10,y2 + 15);
    context.lineTo(x2 - 10,y2 + 25);
    context.fill();
    context.stroke()
}

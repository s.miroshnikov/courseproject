let x = localStorage.getItem('x');
let buttonObjectArray = [], areasObjectArray = [], relationObjectArray = [];
if(x == null) {
    x = 1;
}
else{
    x = localStorage.getItem('x');
    for(let i = 1; i < x; i++) {
        buttonObjectArray[i] = {
            id: `Concept ${i}`,
            name: localStorage.getItem(`name ${i}`),
            pos: localStorage.getItem(`pos ${i}`),
            left: localStorage.getItem(`left ${i}`),
            top: localStorage.getItem(`top ${i}`),
            height: localStorage.getItem(`height ${i}`),
            width: localStorage.getItem(`width ${i}`),
            coordinateX: localStorage.getItem(`CoordinateX ${i}`),
            coordinateY: localStorage.getItem(`CoordinateY ${i}`),
            type: document.getElementById("typeSelect").value
        }
    }
}

let stringConcept, stringArea;

let globalID;

let addConcept = document.getElementById('addConcept');
let deleteAll = document.getElementById('deleteAll')

let modal = document.getElementById("mainModal");
let form = document.getElementById('buttonStorage');

let className = document.getElementById("classCreationModalName");
let classColorChanger = document.getElementById("classColorChanger");

let classCreationModal = document.getElementById('classCreationModal');
let spanClassCreationModal = document.getElementById('closeClassCreationModal');

let saveClass = document.getElementById("saveClass");

let cancelDeleteValue = document.getElementById("cancelDeleteValue");
let valuesOfClass = document.getElementById('valueList');

saveClass.addEventListener("click", addButton);
deleteAll.addEventListener("click", deleteConcepts);

let nameOfClass;

let typeSelect = document.getElementById('typeSelect');

function RGBToHex(rgb) {
    rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);

    return (rgb && rgb.length === 4) ? "#" +
        ("0" + parseInt(rgb[1], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[2], 10).toString(16)).slice(-2) +
        ("0" + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
}

let repeat;
className.onchange = function(){
    for (let j = 1; j < localStorage.getItem('x'); j++) {
        if (className.value.toLowerCase() === localStorage.getItem(`name ${j}`).toLowerCase()   ) {
            className.style.color = '#db0000';
            className.value = 'Имя занято';
            className.setAttribute('disabled', 'disabled');
            setTimeout(function (){
                className.removeAttribute('disabled');
                className.style.color = '#000';
                className.value = '';
                className.focus();
            },600)
            console.log("Повтор");
        }
    }
}

let typeOfSelect;
function addButton() {
    if(document.getElementById("typeSelect").value === "Выберите тип"
        || document.getElementById("typeSelect").value === "Тип не выбран"){
        typeOfSelect = 1;
    }else{
        typeOfSelect = 0;
    }

    if (className.value !== 'Имя занято' && typeOfSelect !== 1) {
        if (className.value !== "") {
            nameOfClass = className.value;
        } else {
            nameOfClass = `Concept${x}`;
        }

        stringConcept = `<button id="Concept ${x}" class="conceptButton">${nameOfClass}</button>`;
        form.insertAdjacentHTML("afterbegin", stringConcept);

        let btn = document.getElementById(`Concept ${x}`);

        btn.style.position = 'absolute';
        btn.style.display = 'block';
        btn.style.left = 65 + 'px';
        btn.style.top = 125 + 'px';
        btn.style.color = '#fff'
        btn.style.background = document.getElementById("classColorChanger").value;

        let el;

        btn.onmousedown = function (e) {
            if (e.which === 1) {
                let coords = getCoords(btn);
                let shiftX = e.pageX - coords.left;
                let shiftY = e.pageY - coords.top;

                btn.style.position = 'absolute';
                moveAt(e);

                btn.style.zIndex = '15';

                function moveAt(e) {
                    btn.style.left = e.pageX - shiftX + 'px';
                    btn.style.top = e.pageY - shiftY + 'px';
                }

                document.onmousemove = function (e) {
                    moveAt(e);

                    connect();
                    moveName();
                };

                btn.onmouseup = function () {
                    el = parseInt(btn.id.match(/\d+/));


                    localStorage.setItem(`pos ${el}`, btn.style.position);
                    localStorage.setItem(`left ${el}`, btn.style.left);
                    localStorage.setItem(`top ${el}`, btn.style.top);
                    localStorage.setItem(`buttonColor ${el}`, btn.style.background);
                    localStorage.setItem(`CoordinateX ${el}`, `${btn.getBoundingClientRect().x}`);
                    localStorage.setItem(`CoordinateY ${el}`, `${btn.getBoundingClientRect().y}`);

                    document.onmousemove = null;
                    btn.style.zIndex = '14';
                    btn.onmouseup = null;
                };

            }
            btn.ondragstart = function () {
                return false;
            };

            function getCoords(elem) {
                let box = elem.getBoundingClientRect();
                return {
                    top: box.top + pageYOffset,
                    left: box.left + pageXOffset
                }
            }
        }

        btn.oncontextmenu = function (event) {
            globalID = parseInt(event.target.id.match(/\d+/));
            console.log(globalID);

            modalValues.value = "";
            if (btn.style.background === "") {
                newColorForm.value = '#2b2b2b';
                newColorForm.background = '#2b2b2b';
            } else {
                newColorForm.value = RGBToHex(btn.style.background);
                newColorForm.background = RGBToHex(btn.style.background)
            }

            n.value = localStorage.getItem(`name ${globalID}`)
            nType.value = "Тип: " + localStorage.getItem(`Type ${globalID}`).toLowerCase();
            katex.render('', document.getElementById('formula'));

            modal.style.display = "block";
            document.body.style.position = 'fixed';
            document.body.overflowY = 'hidden';

            let sortButtonAZ = document.getElementById("sortButtonAZ");
            let sortButtonZA = document.getElementById("sortButtonZA");

            sortButtonAZ.onclick = function () {
                let massiveOfValues = localStorage.getItem(`valueStorage ${globalID}`).split(',');

                massiveOfValues.sort();

                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                for (let i = 0; i < massiveOfValues.length; i++) {
                    let tr = document.createElement("tr");
                    tr.setAttribute('id', `${i + 1}`);
                    let tdName = document.createElement("td");

                    tdName.innerHTML = massiveOfValues[i];
                    tr.appendChild(tdName);
                    valuesOfClass.appendChild(tr);
                }

                localStorage.setItem(`valueStorage ${globalID}`, `${massiveOfValues.sort()}`);
            }
            sortButtonZA.onclick = function () {
                let massiveOfValues = localStorage.getItem(`valueStorage ${globalID}`).split(',');

                massiveOfValues.sort();
                localStorage.setItem(`valueStorage ${globalID}`, `${massiveOfValues.reverse()}`);

                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                for (let i = 0; i < massiveOfValues.length; i++) {
                    let tr = document.createElement("tr");
                    tr.setAttribute('id', `${i + 1}`);
                    let tdName = document.createElement("td");

                    tdName.innerHTML = massiveOfValues[i];
                    tr.appendChild(tdName);
                    valuesOfClass.appendChild(tr);
                }
            }

            while (valuesOfClass.firstChild) {
                valuesOfClass.removeChild(valuesOfClass.firstChild);
            }

            let functions = document.getElementById('function');
            modalValues.value = "";
            if (localStorage.getItem(`Type ${globalID}`) === 'Формульный') {
                addModalValues.textContent = 'Добавить формулу в класс';

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Формулы";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                let font = document.getElementById("valueList");
                font.style.fontSize = '14pt';

                phrase.style.display = 'none'
                functions.style.display = 'flex';

                functions.onclick = function (e) {
                    let type, operations;
                    if(e.target.nextElementSibling !== null) {
                        type = e.target.nextElementSibling.tagName;
                        operations = document.getElementById(e.target.nextElementSibling.id);
                    }else{
                        type = 'zero';
                        operations = 'null';
                    }

                    if(operations!=='null' && operations.classList.contains('show') && type!== 'BUTTON'){
                        operations.classList.remove('show');
                    }else if(operations!=='null' && !operations.classList.contains('show') && type!== 'BUTTON'){
                        for(let i = 1; i<7; i++) {
                            document.getElementById(`functionList${i}`).classList.remove('show');
                        }
                        operations.classList.toggle('show');
                    }

                    if(type === 'BUTTON' || type === 'zero'){
                        stringCreation(e.target.id);
                        for(let i = 1; i<7; i++) {
                            document.getElementById(`functionList${i}`).classList.remove('show');
                        }
                    }
                }

                let modalVal
                modalValues.oninput = function () {
                    modalVal = modalValues.value;

                    katex.render(modalVal, document.getElementById('formula'));
                }

            } else {
                addModalValues.textContent = 'Добавить значение';

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                let font = document.getElementById("valueList");
                font.style.fontSize = '16px';
                phrase.style.display = 'block'
                functions.style.display = 'none';
            }

            if (localStorage.getItem(`Type ${globalID}`) === 'Символьный') {
                if (localStorage.getItem(`valueStorage ${globalID}`) !== null) {
                    let font = document.getElementById("valueList");
                    font.style.fontSize = '16px';
                    let massiveOfValues = localStorage.getItem(`valueStorage ${globalID}`).split(',');
                    console.log(massiveOfValues);

                    for (let i = 0; i < massiveOfValues.length; i++) {
                        let tr = document.createElement("tr");
                        tr.setAttribute('id', `${i + 1}`);
                        let tdName = document.createElement("td");

                        tdName.innerHTML = massiveOfValues[i];
                        tr.appendChild(tdName);
                        valuesOfClass.appendChild(tr);
                    }
                }
            } else if (localStorage.getItem(`Type ${globalID}`) === 'Формульный') {
                if (localStorage.getItem(`valueStorage ${globalID}`) !== null) {
                    let font = document.getElementById("valueList");
                    font.style.fontSize = '14pt';
                    let massiveOfValues = localStorage.getItem(`valueStorage ${globalID}`).split(',');
                    console.log(massiveOfValues);

                    for (let i = 0; i < massiveOfValues.length; i++) {
                        let tr = document.createElement("tr");
                        tr.setAttribute('id', `${i + 1}`);
                        let tdName = document.createElement("td");

                        let modalVal = massiveOfValues[i];

                        tdName.innerHTML = katex.renderToString(modalVal);
                        tr.appendChild(tdName);
                        valuesOfClass.appendChild(tr);
                    }
                }
            }

            addModalValues.onclick = function () {
                console.log(globalID);
                addModalValuesInStorage(globalID);
            }

            changeModalValues.onclick = function (){
                changeModalValueInStorage(globalID);
            }

            valueList.onclick = function (e) {
                if (localStorage.getItem(`Type ${globalID}`)!==`Неструктурированный`) {
                    leftButton(e, globalID);
                }
            }

            valueList.oncontextmenu = function(e){
                rightButton(e, globalID);
                return false;
            }

            formSave.onclick = function () {
                repeat = 0;
                btn.style.background = newColorForm.value;

                for (let j = 1; j < localStorage.getItem('x'); j++) {
                    if (n.value.toLowerCase() === localStorage.getItem(`name ${j}`).toLowerCase() && j!==globalID) {
                        repeat = 1;
                        n.style.color = '#db0000';
                        n.value = 'Имя занято';
                        n.setAttribute('disabled', 'disabled');
                        setTimeout(function (){
                            n.removeAttribute('disabled');
                            n.style.color = '#fff';
                            n.value = '';
                            n.focus();
                        },2000)
                        console.log("Повтор");
                    }
                }
                if (repeat === 0 && n.value !== '' && n.value !== 'Имя занято') {
                    btn.textContent = n.value;
                    localStorage.setItem(`name ${globalID}`, n.value);
                    modal.style.display = "none";
                }

                btn.style.background = newColorForm.value;
                localStorage.setItem(`buttonColor ${globalID}`, newColorForm.value);

                for(let i = 1; i<7; i++) {
                    document.getElementById(`functionList${i}`).classList.remove('show');
                }

                localStorage.setItem(`height ${globalID}`, btn.offsetHeight);
                localStorage.setItem(`width ${globalID}`, btn.offsetWidth);
                document.body.style.position = '';
                document.body.overflowY = '';
                connect();
                moveName();
            }

            deleteClassForm.onclick = function deleteClassButton() {
                localStorage.removeItem(`ConceptId ${globalID}`);
                localStorage.removeItem(`name ${globalID}`);
                localStorage.removeItem(`pos ${globalID}`);
                localStorage.removeItem(`left ${globalID}`);
                localStorage.removeItem(`top ${globalID}`);
                localStorage.removeItem(`height ${globalID}`);
                localStorage.removeItem(`width ${globalID}`);
                localStorage.removeItem(`buttonColor ${globalID}`);
                localStorage.removeItem(`CoordinateX ${globalID}`);
                localStorage.removeItem(`CoordinateY ${globalID}`);
                localStorage.removeItem(`Type ${globalID}`);
                localStorage.removeItem(`valueStorage ${globalID}`);

                for (let i = globalID; i < localStorage.getItem('x'); i++) {

                }

                let xWithout = x;
                localStorage.setItem(`xWithout`, `${xWithout}`);
                x--;
                localStorage.setItem(`x`, `${x}`);
                btn.remove();
                location.reload();
            }

            return false;
        }

        buttonObjectArray[x] = {
            id: `Concept ${x}`,
            name: btn.textContent,
            pos: btn.style.position,
            left: btn.style.left,
            top: btn.style.top,
            height: btn.offsetHeight,
            width: btn.offsetWidth,
            coordinateX: btn.getBoundingClientRect().x,
            coordinateY: btn.getBoundingClientRect().y,
            type: document.getElementById("typeSelect").value,
            color: document.getElementById("classColorChanger").value
        };

        localStorage.setItem(`ConceptId ${x}`, buttonObjectArray[x].id);
        localStorage.setItem(`name ${x}`, buttonObjectArray[x].name);
        localStorage.setItem(`pos ${x}`, buttonObjectArray[x].pos);
        localStorage.setItem(`left ${x}`, buttonObjectArray[x].left);
        localStorage.setItem(`top ${x}`, buttonObjectArray[x].top);
        localStorage.setItem(`height ${x}`, buttonObjectArray[x].height);
        localStorage.setItem(`width ${x}`, buttonObjectArray[x].width);
        localStorage.setItem(`CoordinateX ${x}`, buttonObjectArray[x].coordinateX);
        localStorage.setItem(`CoordinateY ${x}`, buttonObjectArray[x].coordinateY);
        localStorage.setItem(`Type ${x}`, buttonObjectArray[x].type);
        localStorage.setItem(`buttonColor ${x}`, buttonObjectArray[x].color);

        x++;
        localStorage.setItem('x', `${x}`);
        className.value = '';
        classCreationModal.style.display = "none";
        typeSelect.options[0].removeAttribute('selected');

        classCreationModal.style.left = 0 + 'px';
        classCreationModal.style.top = 0 + 'px';
    }else{
        typeSelect.options[0].textContent = "Тип не выбран";
        typeSelect.options[0].setAttribute('selected','selected');
        typeSelect.style.color = `#b11616`;
        setTimeout(function(){
            typeSelect.options[0].setAttribute('selected','selected');
            typeSelect.options[0].textContent = "Выберите тип";
            typeSelect.style.color = `#000000`;
        }, 500);
    }
}

for (let i = 1; i < localStorage.getItem('x'); i++) {
    if(localStorage.getItem(`ConceptId ${i}`) !==null) {
        buttonObjectArray[i] = {
            id: localStorage.getItem(`ConceptId ${i}`),
            name: localStorage.getItem(`name ${i}`),
            pos: localStorage.getItem(`pos ${i}`),
            left: localStorage.getItem(`left ${i}`),
            top: localStorage.getItem(`top ${i}`),
            color: localStorage.getItem(`buttonColor ${i}`),
            height: localStorage.getItem(`height ${i}`),
            width: localStorage.getItem(`width ${i}`),
            coordinateX: localStorage.getItem(`CoordinateX ${i}`),
            coordinateY: localStorage.getItem(`CoordinateY ${i}`),
            type: document.getElementById("typeSelect").value
        }

        stringConcept = `<button id="Concept ${i}" class="conceptButton">${buttonObjectArray[i].name}</button>`;
        form.insertAdjacentHTML("afterbegin", stringConcept);

        let btn = document.getElementById(`Concept ${i}`);

        btn.style.background = buttonObjectArray[i].color;
        btn.textContent = buttonObjectArray[i].name;
        btn.style.position = buttonObjectArray[i].pos;
        btn.style.display = 'block';
        btn.style.left = buttonObjectArray[i].left;
        btn.style.top = buttonObjectArray[i].top;
    }
}

for (let p = 1; p < parseInt(localStorage.getItem('countOfRelation')) + 1; p++){
    if (localStorage.getItem(`RelationName ${p}`) !== null) {
        relationObjectArray[p] = {
            id: p,
            name: localStorage.getItem(`RelationName ${p}`),
            left: localStorage.getItem(`RelationNamePosX ${p}`),
            top: localStorage.getItem(`RelationNamePosY ${p}`)
        }

        stringRelation = `<div id="relationName ${p}" class="relation"></div>`;
        form.insertAdjacentHTML("beforeend", stringRelation);

        let relation = document.getElementById(`relationName ${p}`);

        relation.textContent = relationObjectArray[p].name;
        relation.style.position = "absolute";
        relation.style.left = relationObjectArray[p].left;
        relation.style.top = relationObjectArray[p].top;
    }
}

let modifyClass = document.getElementById("modifyClass");
let classModifyModal = document.getElementById("classModifyModal");
let spanModifyModal = document.getElementById("closeClassModifyModal");

modifyClass.onclick = function (){
    classModifyModal.style.display = "block";
    document.body.style.position = 'fixed';
    document.body.overflowY = 'hidden';

    let classList = document.getElementById("classList");

    while (classList.firstChild) {
        classList.removeChild(classList.firstChild);
    }

    let tr = document.createElement("tr");
    let thName = document.createElement("th");
    let thType = document.createElement("th");

    thName.innerHTML = "Имена классов";
    thType.innerHTML = "Типы классов"
    tr.appendChild(thName);
    tr.appendChild(thType);
    classList.appendChild(tr);

    for(let i = 1; i < localStorage.getItem('x'); i++) {
        let tr = document.createElement("tr");
        tr.setAttribute('id', `${i}`);
        let tdName = document.createElement("td");
        let tdType = document.createElement("td");

        tdName.innerHTML = localStorage.getItem(`name ${i}`);
        tdType.innerHTML = localStorage.getItem(`Type ${i}`);
        tr.appendChild(tdName);
        tr.appendChild(tdType);
        classList.appendChild(tr);
    }

    classList.addEventListener('click', e => {
        let id = e.target.closest('tr').id;
        console.log(id);

        if (id) {
            classModifyModal.style.display = "none";
            modal.style.display = "block";
            n.value = localStorage.getItem(`name ${id}`);
            nType.value = "Тип: " + localStorage.getItem(`Type ${id}`).toLowerCase();

            let valuesOfClass = document.getElementById('valueList');

            while (valuesOfClass.firstChild) {
                valuesOfClass.removeChild(valuesOfClass.firstChild);
            }

            let functions = document.getElementById('function');
            modalValues.value = "";
            if(localStorage.getItem(`Type ${id}`) === 'Формульный') {
                addModalValues.textContent = 'Добавить формулу в класс';
                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Формулы";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                katex.render('', document.getElementById('formula'));

                phrase.style.display = 'none';
                functions.style.display = 'flex';

                functions.onclick = function(e){
                    let type, operations;
                    if(e.target.nextElementSibling !== null) {
                        type = e.target.nextElementSibling.tagName;
                        operations = document.getElementById(e.target.nextElementSibling.id);
                    }else{
                        type = 'zero';
                        operations = 'null';
                    }

                    if(operations!=='null' && operations.classList.contains('show') && type!== 'BUTTON'){
                        operations.classList.remove('show');
                    }else if(operations!=='null' && !operations.classList.contains('show') && type!== 'BUTTON'){
                        for(let i = 1; i<7; i++) {
                            document.getElementById(`functionList${i}`).classList.remove('show');
                        }
                        operations.classList.toggle('show');
                    }

                    if(type === 'BUTTON' || type === 'zero'){
                        stringCreation(e.target.id);
                        for(let i = 1; i<7; i++) {
                            document.getElementById(`functionList${i}`).classList.remove('show');
                        }
                    }
                }

                let modalVal
                modalValues.oninput = function () {
                    modalValues.value = modalValues.value.replace('*', `⨯`)
                    modalVal = modalValues.value;

                    katex.render(modalVal, document.getElementById('formula'));
                }

            }else{
                addModalValues.textContent = 'Добавить значение';

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                let font = document.getElementById("valueList");
                font.style.fontSize = '16px';
                phrase.style.display = 'block'
                functions.style.display = 'none';
            }

            if(localStorage.getItem(`Type ${id}`) === 'Символьный') {
                if (localStorage.getItem(`valueStorage ${id}`) !== null) {
                    addModalValues.textContent = 'Добавить значение';
                    let massiveOfValues = localStorage.getItem(`valueStorage ${id}`).split(',');
                    console.log(massiveOfValues);

                    for (let i = 0; i < massiveOfValues.length; i++) {
                        let tr = document.createElement("tr");
                        tr.setAttribute('id', `${i + 1}`);
                        let tdName = document.createElement("td");

                        tdName.innerHTML = massiveOfValues[i];
                        tr.appendChild(tdName);
                        valuesOfClass.appendChild(tr);
                    }
                }
            }else if(localStorage.getItem(`Type ${id}`) === 'Формульный') {
                if (localStorage.getItem(`valueStorage ${id}`) !== null) {
                    addModalValues.textContent = 'Добавить формулу в класс';
                    let font = document.getElementById("valueList");
                    font.style.fontSize = '14pt';

                    let massiveOfValues = localStorage.getItem(`valueStorage ${id}`).split(',');
                    console.log(massiveOfValues);

                    for (let i = 0; i < massiveOfValues.length; i++) {
                        let tr = document.createElement("tr");
                        tr.setAttribute('id', `${i + 1}`);
                        let tdName = document.createElement("td");


                        let modalVal = massiveOfValues[i];

                        tdName.innerHTML = katex.renderToString(modalVal);
                        tr.appendChild(tdName);
                        valuesOfClass.appendChild(tr);
                    }
                }
            }

            addModalValues.onclick = function () {
                console.log(id);
                addModalValuesInStorage(id);
            }

            changeModalValues.onclick = function (){
                changeModalValueInStorage(id);
            }

            let btnClass = document.getElementById(`Concept ${id}`);

            if (btnClass.style.background === "") {
                newColorForm.value = '#2b2b2b';
                newColorForm.background = '#2b2b2b';
            } else {
                newColorForm.value = RGBToHex(btnClass.style.background);
                newColorForm.background = RGBToHex(btnClass.style.background)
            }


            let sortButtonAZ = document.getElementById("sortButtonAZ");
            let sortButtonZA = document.getElementById("sortButtonZA");

            sortButtonAZ.onclick = function () {
                let massiveOfValues = localStorage.getItem(`valueStorage ${id}`).split(',');

                massiveOfValues.sort();

                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                for (let i = 0; i < massiveOfValues.length; i++) {
                        let tr = document.createElement("tr");
                        let tdName = document.createElement("td");

                        tdName.innerHTML = massiveOfValues[i];
                        tr.appendChild(tdName);
                        valuesOfClass.appendChild(tr);
                    }

                localStorage.setItem(`valueStorage ${id}`, `${massiveOfValues.sort()}`);
            }
            sortButtonZA.onclick = function () {
                let massiveOfValues = localStorage.getItem(`valueStorage ${id}`).split(',');

                massiveOfValues.sort();
                localStorage.setItem(`valueStorage ${id}`, `${massiveOfValues.reverse()}`);

                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                let tr = document.createElement("tr");
                let thName = document.createElement("th");

                thName.innerHTML = "Значения";
                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                for (let i = 0; i < massiveOfValues.length; i++) {
                    let tr = document.createElement("tr");
                    let tdName = document.createElement("td");

                    tdName.innerHTML = massiveOfValues[i];
                    tr.appendChild(tdName);
                    valuesOfClass.appendChild(tr);
                }
            }

            formSave.onclick = function () {
                rep = 0;
                btnClass.style.background = newColorForm.value;

                for (let j = 1; j < localStorage.getItem('x'); j++) {
                    if (j!==id && id!==1 && n.value.toLowerCase() === localStorage.getItem(`name ${j}`).toLowerCase()) {
                        repeat = 1;
                        n.style.color = '#db0000';
                        n.value = 'Имя занято';
                        n.setAttribute('disabled', 'disabled');
                        setTimeout(function (){
                            n.removeAttribute('disabled');
                            n.style.color = '#fff';
                            n.value = '';
                            n.focus();
                        },2000)
                        console.log("Повтор");
                    }
                }
                if (rep === 0 && n.value !=='' && n.value !=='Имя занято') {
                    btnClass.textContent = n.value;
                    localStorage.setItem(`name ${id}`, n.value);
                    modal.style.display = "none";
                }

                for(let i = 1; i<7; i++) {
                    document.getElementById(`functionList${i}`).classList.remove('show');
                }

                localStorage.setItem(`height ${id}`, btnClass.offsetHeight);
                localStorage.setItem(`width ${id}`, btnClass.offsetWidth);
                localStorage.setItem(`buttonColor ${id}`, newColorForm.value);
                connect();
                moveName();
            }

            deleteClassForm.onclick = function deleteClassButton() {
                localStorage.removeItem(`Concept ${id}`);
                localStorage.removeItem(`name ${id}`);
                localStorage.removeItem(`pos ${id}`);
                localStorage.removeItem(`left ${id}`);
                localStorage.removeItem(`top ${id}`);
                localStorage.removeItem(`height ${id}`);
                localStorage.removeItem(`width ${id}`);
                localStorage.removeItem(`buttonColor ${id}`);
                localStorage.removeItem(`CoordinateX ${id}`);
                localStorage.removeItem(`CoordinateY ${id}`);
                localStorage.removeItem(`Type ${id}`);
                localStorage.removeItem(`valueStorage ${id}`);


                let xWithout = x;
                localStorage.setItem(`xWithout`, `${xWithout}`);
                x--;
                localStorage.setItem(`x`, `${x}`);
                btnClass.remove();
                location.reload();
            }

            return false;
        }
    });

    valueList.onclick = function (e) {
        if (localStorage.getItem(`Type ${id}`)!==`Неструктурированный`) {
            leftButton(e, id);
        }
    }

    valueList.oncontextmenu = function(e) {
        rightButton(e, id);
        return false;
    }
}

let valueList = document.getElementById("valueList");
let spanDeleteModal = document.getElementById("closeDeleteModal");
let deleteModal = document.getElementById("deleteModal");


let valueLeftId;
function leftButton(e, numberId){
    valueLeftId = e.target.closest('tr').id;
    console.log(valueLeftId);

    changeModalValues.style.display = 'inline-block';
    let massiveOfModalElements = localStorage.getItem(`valueStorage ${numberId}`).split(',');

    modalValues.value = massiveOfModalElements[valueLeftId-1];

    localStorage.setItem(`valueStorage ${numberId}`, `${massiveOfModalElements}`);
};

function rightButton(e, numberId){
    let valueId = e.target.closest('tr').id;
    console.log(valueId);

    if(valueId){
        let deleteValue = document.getElementById("deleteValue");

        modal.style.display = "none";
        deleteModal.style.display = "block";

        if(localStorage.getItem(`valueStorage ${numberId}`).split(',')[valueId - 1].match(/[а-я]/gi).length===0) {
            katex.render(localStorage.getItem(`valueStorage ${numberId}`).split(',')[valueId - 1], document.getElementById('deleteModalName'));
        }else{
            document.getElementById('deleteModalName').textContent = localStorage.getItem(`valueStorage ${numberId}`).split(',')[valueId - 1];
        }

        cancelDeleteValue.onclick = function (){
            modal.style.display = `block`;
            deleteModal.style.display = "none";
        }


        let tr = document.createElement("tr");
        let thName = document.createElement("th");
        let tP;

        if(localStorage.getItem(`Type ${numberId}`) === "Символьный"){
            thName.innerHTML = "Значения";
            tP = 'name';
        }else if(localStorage.getItem(`Type ${numberId}`) === "Формульный"){
            thName.innerHTML = "Формульный";
            tP = 'formula';
        }

        deleteValue.onclick = function () {
            let massiveOfValues = localStorage.getItem(`valueStorage ${numberId}`).split(',');
            console.log(massiveOfValues[0]);

            if(massiveOfValues.length!==1) {
                massiveOfValues.splice(valueId-1, 1);

                massiveOfValues.filter(n => n);

                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                for (let i = 0; i < massiveOfValues.length; i++) {
                    let tr = document.createElement("tr");
                    tr.setAttribute('id', `${i + 1}`);
                    let tdName = document.createElement("td");

                    if(tP==='formula') {
                        tdName.innerHTML = katex.renderToString(massiveOfValues[i]);
                    }else if(tP==='name'){
                        tdName.innerHTML = massiveOfValues[i];
                    }

                    tr.appendChild(tdName);
                    valuesOfClass.appendChild(tr);
                }

                modal.style.display = "block";
                deleteModal.style.display = "none";
                localStorage.setItem(`valueStorage ${numberId}`, `${massiveOfValues}`);
            }else if(massiveOfValues.length===1){
                while (valuesOfClass.firstChild) {
                    valuesOfClass.removeChild(valuesOfClass.firstChild);
                }

                tr.appendChild(thName);
                valuesOfClass.appendChild(tr);

                localStorage.removeItem(`valueStorage ${numberId}`);
                modal.style.display = "block";
                deleteModal.style.display = "none";
            }
        }

        spanDeleteModal.onclick = function() {
            document.body.style.position = '';
            document.body.style.overflowY = '';
            modal.style.display = "block";
            deleteModal.style.display = "none";
        }
    }
}

addConcept.onclick = function (){
    typeSelect.options[0].setAttribute('selected', 'selected');

    classCreationModal.style.display = "block";
    classColorChanger.value = '#9a9aff';
    className.focus();
}
spanClassCreationModal.onclick = function() {
    classCreationModal.style.left = 0 + 'px';
    classCreationModal.style.top = 0 + 'px';
    typeSelect.options[0].removeAttribute('selected');
    classCreationModal.style.display = "none";
}
spanModifyModal.onclick = function() {
    classModifyModal.style.display = "none";
    document.body.style.position = '';
    document.body.overflowY = '';
}

classCreationModal.onmousedown = function (e) {
    if (e.which === 1) {
        let coords = getCoords(classCreationModal);
        let shiftX = e.pageX - coords.left;
        let shiftY = e.pageY - coords.top;

        moveAt(e);

        classCreationModal.style.zIndex = '1000';

        function moveAt(e) {
            classCreationModal.style.left = e.pageX - shiftX + 'px';
            classCreationModal.style.top = e.pageY - shiftY + 'px';
        }

        document.onmousemove = function (e) {
            if((e.x - 480 > classCreationModal.getBoundingClientRect().x && e.y - 100 > classCreationModal.getBoundingClientRect().y)
                && (e.x - 480 < classCreationModal.getBoundingClientRect().x + 960 && e.y - 100 < classCreationModal.getBoundingClientRect().y + 49)) {
            moveAt(e);
            }
        };

        classCreationModal.onmouseup = function () {
            document.onmousemove = null;
            classCreationModal.onmouseup = null;
        };

    }
    classCreationModal.ondragstart = function () {
        return false;
    };

    function getCoords(elem) {
        let box = elem.getBoundingClientRect();
        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        }
    }
}

modal.onmousedown = function (e) {
    if (e.which === 1) {
        let coords = getCoords(modal);
        let shiftX = e.pageX - coords.left;
        let shiftY = e.pageY - coords.top;

        moveAt(e);

        modal.style.zIndex = '1000';

        function moveAt(e) {
            modal.style.left = e.pageX - shiftX + 'px';
            modal.style.top = e.pageY - shiftY + 'px';
        }

        document.onmousemove = function (e) {
            if((e.x - 480 > modal.getBoundingClientRect().x && e.y - 100 > modal.getBoundingClientRect().y)
                && (e.x - 480 < modal.getBoundingClientRect().x + 960 && e.y - 100 < modal.getBoundingClientRect().y + 68)) {
                moveAt(e);
            }
        };

        modal.onmouseup = function () {
            document.onmousemove = null;
            modal.onmouseup = null;
        };

    }
    modal.ondragstart = function () {
        return false;
    };

    function getCoords(elem) {
        let box = elem.getBoundingClientRect();
        return {
            top: box.top + pageYOffset,
            left: box.left + pageXOffset
        }
    }
}

function deleteConcepts(){
    localStorage.clear();
    for(let i = 1; i < arrayOfLinks.length; i++) {
        arrayOfLinks[i].clear();
    }
    location.reload();
}




